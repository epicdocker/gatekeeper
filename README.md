# Keycloak Gatekeeper - Docker-Image

Build Keycloak Gatekeeper as docker image.  
Uses the repository [jboss-dockerfiles/keycloak](https://github.com/jboss-dockerfiles/keycloak) and the tags to create the version.  
Pipeline runs once a week and automatically builds the last tag.

---

## Troubleshooting

If an error page is displayed after authentication and the following is displayed in the gatekeeper log:

`unable to verify the id token {"error": "oidc: JWT claims invalid: invalid claims, 'aud' claim and 'client_id' do not match, aud=account, client_id=proxy"}
`

__Configure audience in Keycloak__
* Add realm or configure existing
* Add client my-app or use existing
* Goto to the newly added "Client Scopes" menu
  * Add Client scope 'good-service'
  * Within the settings of the 'good-service' goto Mappers tab
    * Create Protocol Mapper 'my-app-audience'
      * Name: my-app-audience
      * Choose Mapper type: Audience
      * Included Client Audience: my-app
      * Add to access token: on
* Configure client my-app in the "Clients" menu
  * Client Scopes tab in my-app settings
  * Add available client scopes "good-service" to assigned default client scopes

Source: https://stackoverflow.com/a/53627747/2209423

---
